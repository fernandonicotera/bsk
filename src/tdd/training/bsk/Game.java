package tdd.training.bsk;
import java.util.*;

public class Game {
	
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	
	private ArrayList<Frame> frames = new ArrayList<Frame>();
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if (this.frames.size()<10) {
		this.frames.add(frame);
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
	if(index<10) {
		return this.frames.get(index);
	}
		throw new BowlingException();
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int i=0; i<10; i++) {
			//SPARE
			if(frames.get(i).isSpare() && i<9) {
				int bonus=frames.get(i+1).getFirstThrow();
				frames.get(i).setBonus(bonus);
			}
			
			//STRIKE
			if(frames.get(i).isStrike() && i<9) {
				int bonus=0;
				//SUBSEQUENT STRIKES
				if(frames.get(i+1).isStrike() && i<8) {
					bonus=frames.get(i+1).getFirstThrow() + frames.get(i+2).getFirstThrow();
				}
				else{
					bonus= frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow();
				}
				frames.get(i).setBonus(bonus);
			}
			
			
			
			score = score+frames.get(i).getScore();
		}
		
		//BONUS THROWS
		if(frames.get(9).isSpare()) {
			score = score+ this.getFirstBonusThrow();
		}
		if(frames.get(9).isStrike()) {
			score = score + this.getFirstBonusThrow();
			score = score + this.getSecondBonusThrow();
		}
		//PERFECT GAME
		if(
		this.frames.get(0).isStrike()&&
		this.frames.get(1).isStrike()&&
		this.frames.get(2).isStrike()&&
		this.frames.get(3).isStrike()&&
		this.frames.get(4).isStrike()&&
		this.frames.get(5).isStrike()&&
		this.frames.get(6).isStrike()&&
		this.frames.get(7).isStrike()&&
		this.frames.get(8).isStrike()&&
		this.frames.get(9).isStrike()&&
		this.firstBonusThrow==10 &&
		this.secondBonusThrow==10
		) {
			score=300;
		}
		return score;	
	}

}
