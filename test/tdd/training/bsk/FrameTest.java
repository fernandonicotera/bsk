package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void firstThrowShouldReturn2Pins() throws Exception{
		Frame frame= new Frame(2,4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void secondThrowShouldReturn4Pins() throws Exception{
		Frame frame= new Frame(2,4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	
	public void scoreShouldBe8() throws Exception{
		Frame frame = new Frame(2,6);
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void shouldBeSpare() throws Exception{
		Frame frame = new Frame(5,5);
		assertEquals(true, frame.isSpare());
	}
	
	@Test
	public void GetSetBonusTest() throws Exception{
		Frame frame = new Frame(5,5);
		frame.setBonus(10);
		assertEquals(10, frame.getBonus());
	}
	
	@Test
	public void shouldBeStrike() throws Exception{
		Frame frame = new Frame(10,0);
		assertEquals(true, frame.isStrike());
	}
	
	@Test
	public void spareShouldNotBeStrike() throws Exception{
		Frame frame = new Frame(5,5);
		assertEquals(false , frame.isStrike());
	}
	
	
}
