package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {
	
	
	
	public Game gameFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		return game;
	}
	
	public Game spareGameFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		return game;
	}
	
	public Game strikeGameFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		return game;
	}
	
	public Game strikeSpareGameFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		return game;
	}
	
	public Game subsequentStrikesFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		return game;
	}
	
	public Game subsequentSparesFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		return game;
	}
	
	public Game spareLastFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		
		game.setFirstBonusThrow(7);
		return game;
	}
	
	public Game strikeLastFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		return game;
	}
	
	public Game perfectFixture() throws Exception{
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		return game;
	}
	
	@Test
	public void getFirstFrameFirstThrowTest() throws Exception{
		Game game=gameFixture();
		Frame frame=game.getFrameAt(0);
		assertEquals(1, frame.getFirstThrow());
	}
	
	@Test
	public void getFirstFrameSecondThrowTest() throws Exception{
		Game game=gameFixture();
		Frame frame=game.getFrameAt(0);
		assertEquals(5, frame.getSecondThrow());
	}
	
	@Test
	public void getGameScore() throws Exception{
		Game game=gameFixture();
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void calculateSpareGame() throws Exception{
		Game game=spareGameFixture();
		assertEquals(88, game.calculateScore());
		
	}
	
	@Test
	public void calculateStrikeGame() throws Exception{
		Game game=strikeGameFixture();
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void calculateStrikeSpareGame() throws Exception{
		Game game=strikeSpareGameFixture();
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void calculateSubsequentStrikes() throws Exception{
		Game game=subsequentStrikesFixture();
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void calculateSubsequentSpares() throws Exception{
		Game game=subsequentSparesFixture();
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void setFirstBonusTest() throws Exception{
		Game game= gameFixture();
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	
	@Test
	public void calculateSpareLast() throws Exception{
		Game game=spareLastFixture();
		assertEquals(90, game.calculateScore());
		
	}
	
	@Test
	public void setSecondBonusTest() throws Exception{
		Game game= gameFixture();
		game.setSecondBonusThrow(7);
		assertEquals(7, game.getSecondBonusThrow());
	}
	
	@Test
	public void calculateStrikeLast() throws Exception{
		Game game= strikeLastFixture();
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void perfectGameTest() throws Exception{
		Game game= perfectFixture();
		assertEquals(300, game.calculateScore());
	}
}
